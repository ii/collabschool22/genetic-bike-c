from simulation import simulate
from ga import GA
import numpy as np

def main():
    bounds = np.zeros([8,2])
    bounds[:,1] = 5
    ga_engine = GA(simulate, bounds, n_bits=16, n_iter=10, n_pop=10, r_cross=0.9, r_mut=.0078)
    solution, obj_val = ga_engine.run()

if __name__ == "__main__":
    main()
