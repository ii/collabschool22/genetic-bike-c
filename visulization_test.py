# f(x,y) = - ((x+2y-7)^² + (2x+y-5)^2)
# Optimmum point f(x=1, y= 3) = 0 

from ga import GA

import matplotlib.pyplot as plt

def objective_2(x):
    return -((x[0]+2*x[1]-7)**2+(2*x[0]+x[1]-5)**2)

# define range for input
bounds = [[-5.0, 5.0], [-5.0, 5.0]]

# define the total iterations
n_iter = 10

# bits per variable
n_bits = 16

# define the population size
n_pop = 100

# crossover rate
r_cross = 0.9

# mutation rate
r_mut = 1.0 / (float(n_bits) * len(bounds))

# perform the genetic algorithm search
ga_engine = GA(objective_2, bounds, n_bits, n_iter, n_pop, r_cross, r_mut)

# output of ga_engine.run will give two output, the first is list of best result, second is the log
best_res, record = ga_engine.run()



#list_all_datapoint
# visulize the record
ga_engine.visualize_log(ga_record=record)

plt.savefig("test.jpg")