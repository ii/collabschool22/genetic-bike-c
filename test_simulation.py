from simulation import *
from bike import Bike


def test_timestep_gravity():
    bike = Bike([[0, 0.1], [1, 0.1], [0, 1], [1, 1]])
    ground = lambda x: 0
    old_points = bike.points.copy()
    new_points = old_points
    new_points[:,1] -= 9.81
    timestep(bike, ground, 1)
    assert np.allclose(bike.points, new_points)

def test_simulate_distance_travelled():
    distance = simulate([[0, 0], [1, 0], [0, 1], [1, 1]], t_step=1, t_total=1)
    assert np.isclose(distance, 250)
