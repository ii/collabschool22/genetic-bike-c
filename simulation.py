from bike import Bike
import numpy as np
import pandas as pd

def timestep(bike, ground, t_step):
    bike.ground_intersection(ground)
    forces = bike.force(ground)
    bike.velocities[:,0] = (forces[:,0]/bike.masses) * t_step
    bike.velocities[:,1] = (forces[:,1]/bike.masses) * t_step
    bike.points[:,0] += bike.velocities[:,0] * t_step
    bike.points[:,1] += bike.velocities[:,1] * t_step

def simulate(initial_points, t_step=0.002, t_total=100, t_out=1, save=False):
    bike = Bike(initial_points)
    ground = lambda x: 0
    x_initial = bike.get_position()
    if (save): out_data = pd.DataFrame(columns=['t', 'x1', 'y1', 'x2', 'y2', 'x3', 'y3', 'x4', 'y4'])

    for t in np.arange(0, t_total, t_step):
        timestep(bike, ground, t_step)
        if (bike.check_failed(ground)):
            break
        if (save and t%t_out==0):
            data = {'t': t,
                    'x1': bike.points[0,0], 'y1': bike.points[0,1],
                    'x2': bike.points[1,0], 'y2': bike.points[1,1],
                    'x3': bike.points[2,0], 'y3': bike.points[2,1],
                    'x4': bike.points[3,0], 'y4': bike.points[3,1],
                    }
            out_data = out_data.append(data, ignore_index=True)

    if (save):
        out_data['t'] = out_data['t'].map(lambda t: f'{t:g}') # Format timestep differently
        out_data.to_csv('simulation.txt', sep=',', index=False, float_format='%.05e')

    x_final = bike.get_position()
    distance_traveled = x_final - x_initial
    missed_time_since_failing = t - (t_total-1)    # 0 if bike didn't fall
    return distance_traveled + 1e5*(missed_time_since_failing)
