from numpy.random import randint
from numpy.random import rand
import numpy as np
import matplotlib.pyplot as plt

"""NB: started from the example published by Jason Brownlee (2021),
on the machinelearningmastery.com blog"""

class GA:

    def __init__(self, objective, bounds, n_bits, n_iter, n_pop, r_cross, r_mut):
        assert callable(objective)
        assert 0 <= r_cross <= 1
        assert 0 <= r_mut <= 1
        self.objective = objective
        self.bounds = bounds
        self.n_bits = n_bits
        self.n_iter = n_iter
        self.n_pop = n_pop
        self.r_cross = r_cross
        self.r_mut = r_mut

    # decode bitstring to numbers
    def decode(self, bitstring):
        decoded = list()
        largest = 2**self.n_bits
        for i in range(len(self.bounds)):
            # extract the substring
            start, end = i * self.n_bits, (i * self.n_bits)+self.n_bits
            substring = bitstring[start:end]
            # convert bitstring to a string of chars
            chars = ''.join([str(s) for s in substring])
            # convert string to integer
            integer = int(chars, 2)
            # scale integer to desired range
            value = self.bounds[i][0] + (integer/largest) * (self.bounds[i][1] - self.bounds[i][0])
            # store
            decoded.append(value)
        return decoded

    # tournament selection
    def selection(self, pop, scores, k=3):
        # first random selection
        selection_ix = randint(len(pop))
        for ix in randint(0, len(pop), k-1):
            # check if better (e.g. perform a tournament)
            if scores[ix] > scores[selection_ix]:
                selection_ix = ix
        return pop[selection_ix]

    # crossover two parents to create two children
    def crossover(self, p1, p2):
        # children are copies of parents by default
        c1, c2 = p1.copy(), p2.copy()
        # check for recombination
        if rand() < self.r_cross:
            # select crossover point that is not on the end of the string
            pt = randint(1, len(p1)-2)
            # perform crossover
            c1 = p1[:pt] + p2[pt:]
            c2 = p2[:pt] + p1[pt:]
        return [c1, c2]

    # mutation operator
    def mutation(self, bitstring):
        for i in range(len(bitstring)):
            # check for a mutation
            if rand() < self.r_mut:
                # flip the bit
                bitstring[i] = 1 - bitstring[i]

    def run(self):
        # initial population of random bitstring
        pop = [randint(0, 2, self.n_bits*len(self.bounds)).tolist() for _ in range(self.n_pop)]
        # keep track of best solution
        best, best_eval = 0, self.objective(self.decode(pop[0]))
        # enumerate generations
        score = []
        list_best = []
        for gen in range(self.n_iter):
            # decode population
            decoded = [self.decode(p) for p in pop]
            # evaluate all candidates in the population
            scores = [self.objective(d) for d in decoded]
            # check for new best solution
            for i in range(self.n_pop):
                if scores[i] > best_eval:
                    best, best_eval = pop[i], scores[i]
                    print(">iteration %d: new best f(%s) = %f" % (gen,  decoded[i], scores[i]))
            
            # recording the best individual in each iteration

            list_best.append([gen, self.decode(best),best_eval, scores])
            
            # select parents
            selected = [self.selection(pop, scores) for _ in range(self.n_pop)]
            # create the next generation
            children = list()
            for i in range(0, self.n_pop, 2):
                # get selected parents in pairs
                p1, p2 = selected[i], selected[i+1]
                # crossover and mutation
                for c in self.crossover(p1, p2):
                    # mutation
                    self.mutation(c)
                    # store for next generation
                    children.append(c)
            # replace population
            pop = children
            print(">iteration %d complete, best objective value = %f" % (gen,  best_eval))
        print('Done!')
        decoded = self.decode(best)
        print('f(%s) = %f' % (decoded, best_eval))
        return [decoded, best_eval], list_best
    
    def visualize_log(self, ga_record):
         
        best_value_list = np.zeros(self.n_iter)

        for i, each_gene in enumerate(ga_record):
    
            best_value_list[i] = each_gene[2]

        plt.plot(np.arange(self.n_iter), best_value_list)
        plt.xlabel("Iteration Number")
        plt.ylabel("Score of Best Design")
        plt.savefig('ga_waterfall.png', dpi=400)
        plt.show()

        list_all_datapoint = []

        for i, elemnet in enumerate(ga_record):
            list_all_datapoint.append(elemnet[3])
    
        plt.boxplot(list_all_datapoint)
        plt.xlabel("Iteration Number")
        plt.ylabel("Box Plot of Score (at each generation)")
        plt.savefig('ga_boxplot.png', dpi=400)
        plt.show()
        