from plot_bike import *
from bike import Bike
import numpy as np

def test_plot_bike():
	bike = Bike([[0, 0], [0, 1], [1, 0], [1, 1]])
	fig = plot_bike(bike)
	#plt.show(block=False)
	#plt.pause(1)
	#plt.close()
	assert True

def test_load_data_no_ground(tmp_path):
    data_file = tmp_path / 'simulation.txt'
    CONTENT = "t,x1,y1,x2,y2,x3,y3,x4,y4\n0,0,0,1,0,0,1,1,1"
    data_file.write_text(CONTENT)
    times, points, ground = load_data(str(data_file))
    assert np.allclose(points[0], [[0, 0], [1, 0], [0, 1], [1, 1]])
    assert np.isclose(ground(0), 0)
    assert np.isclose(ground(1), 0)

def test_load_data(tmp_path):
    data_file = tmp_path / 'simulation.txt'
    CONTENT = "GROUND sin 0.5 1\nt,x1,y1,x2,y2,x3,y3,x4,y4\n0,0,0,1,0,0,1,1,1"
    data_file.write_text(CONTENT)
    times, points, ground = load_data(str(data_file))
    assert np.allclose(points[0], [[0, 0], [1, 0], [0, 1], [1, 1]])
    assert np.isclose(ground(0), -0.5)
    assert np.isclose(ground(np.pi/2), 0)
