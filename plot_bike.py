import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from bike import Bike 
import matplotlib.animation as animation


def initialise_ground(params):
    if (params is None):
        ground = lambda x: np.zeros_like(x)
    elif (params[0] == 'sin'):
        ground = lambda x: float(params[1]) * (np.sin(float(params[2])*x) - 1)
    return ground


def load_data(data_file):
    ground_params = None
    with open(data_file, 'r') as f:
        for line in f:
            values = line.split()
            if (values[0] == 'GROUND'):
                ground_params = values[1:]
    ground = initialise_ground(ground_params)
    header = 0 if (ground_params is None) else 1
    data = pd.read_csv(data_file, header=header)
    t = data['t'].to_numpy()
    points = data[['x1','y1','x2','y2','x3','y3','x4','y4']].to_numpy()
    points = points.reshape((-1,4,2))
    return t, points, ground


def plot_bike(bike, ax=None):
    if (ax is None): ax = plt.gca()
    ms = 20
    for pair in bike.pairs:
        ax.plot(bike.points[pair,0], bike.points[pair,1], c='grey')
    ax.plot(bike.points[0,0], bike.points[0,1], 'ro', ms=ms)
    ax.plot(bike.points[1:,0], bike.points[1:,1], 'ko', ms=ms)


def plot_ground(ground, ax=None):
    if (ax is None): ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    x = np.linspace(xlim[0], xlim[1], 100)
    y_ground = ground(x)
    y_lower = np.full_like(y_ground, -np.inf)
    y_lower = np.full_like(y_ground, ylim[0])
    ax.fill_between(x, y_ground, y_lower, fc='grey')
    # ax.set_xlim(xlim)
    # ax.set_ylim(ylim)


def animate_bike(fig=None, ax=None):
    if (fig is None): fig = plt.gcf()
    if (ax is None): ax = plt.gca()

    times, points, ground = load_data('simulation.txt')
    xlim = [np.min(points[:,:,0]), np.max(points[:,:,0])]
    ylim = [np.min(points[:,:,1])-0.5, np.max(points[:,:,1])+0.5]

    
    def animate(frame):
        ax.clear()
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_aspect('equal')
        ax.autoscale(False)
        bike = Bike(points[frame])
        plot_bike(bike, ax)
        plot_ground(ground, ax)
    
    ani = animation.FuncAnimation(fig, animate, frames=len(times), repeat=False)
    plt.show()
