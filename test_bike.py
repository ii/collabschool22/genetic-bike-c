from bike import *
import numpy as np

def test_bike_class_exists():
    bike = Bike(np.zeros((4,2)))
    bike.points
    bike.masses
    bike.rigidity
    bike.lengths

def test_bike_mass_rigidity_initialised_properly():
    bike = Bike(np.zeros((4,2)))
    assert np.all(bike.masses == [1, 1, 40, 40])
    assert np.all(bike.velocities == [[0,0], [0,0], [0,0], [0,0]])
    assert bike.rigidity == 1e4

def test_bike_length_initialises_properly():
    bike = Bike([[0, 0], [0, 1], [1, 0], [1, 1]])
    assert np.all(bike.points == [[0, 0], [0, 1], [1, 0], [1, 1]])
    assert np.all(bike.lengths == [1, 1, np.sqrt(2), np.sqrt(2), 1, 1])

def test_bike_length_initialises_1d():
    bike = Bike([0, 0, 0, 1, 1, 0, 1, 1])
    assert np.all(bike.points == [[0, 0], [0, 1], [1, 0], [1, 1]])
    assert np.all(bike.lengths == [1, 1, np.sqrt(2), np.sqrt(2), 1, 1])

def test_bike_pair_list():
    bike = Bike([[0, 0], [0, 1], [1, 0], [1, 1]])
    assert np.all(bike.pairs == [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]])

def test_bike_springs():
    bike = Bike([[0, 0], [0, 1], [1, 0], [1, 1]])
    bike.lengths[0] = 0.9
    yforce = 0.1 * bike.rigidity
    assert np.allclose(bike.spring_force(), [[0, yforce], [0, -yforce], [0, 0], [0, 0]])

def test_forces():
    bike = Bike([[0, 0.1], [1, 0.1], [0, 1], [1, 1]])
    ground = lambda x: 0
    forces = bike.force(ground)
    g = 9.81
    assert np.allclose(forces, [[0, -bike.masses[0]*g], [0, -bike.masses[1]*g], [0, 0-bike.masses[2]*g], [0, -bike.masses[3]*g]])

def test_ground_intersection():
    ground = lambda x: 1 - x
    bike = Bike([[0, 0], [1, 0], [0, 1], [1, 1]])
    bike.ground_intersection(ground)
    assert np.allclose(bike.points[0], [0,1])
    assert np.allclose(bike.points[1], [1,0])

def test_bike_position():
    bike = Bike([[0, 0], [1, 0], [0, 0.5], [1, 1]])
    xpos = bike.get_position()
    assert xpos == 0.5

def test_in_contact():
    bike = Bike([[0, 0], [1, 0], [0, -0.5], [1, 1]])
    ground = lambda x: 0
    in_contact = bike.in_contact(ground)
    np.testing.assert_equal(in_contact, [True, True, True, False])

def test_bike_has_driving_force():
    bike = Bike([[0, 0], [1, 0], [0, -0.5], [1, 1]], driving=1)
    assert bike.driving == 1

def test_driving_force_if_in_contact():
    bike = Bike([[0, 0], [1, 0], [0, -0.5], [1, 1]], driving=1)
    ground = lambda x: 0
    forces = bike.driving_force(ground)
    assert np.allclose(forces, [[1,0], [0,0], [0,0], [0,0]])

def test_driving_force_if_not_in_contact():
    bike = Bike([[0, 0], [1, 0], [0, -0.5], [1, 1]])
    ground = lambda x: -1
    forces = bike.driving_force(ground)
    assert np.allclose(forces, [[0,0], [0,0], [0,0], [0,0]])

def test_bike_failed_1():
    bike = Bike([[0, 0], [1, 0], [0, 0], [1, 0]])
    ground = lambda x: 0
    assert bike.check_failed(ground)

def test_bike_failed_2():
    bike = Bike([[0, 0], [1, 0], [0, -1], [1, 1]])
    ground = lambda x: 0
    assert bike.check_failed(ground)

def test_bike_failed_3():
    bike = Bike([[0, 0], [1, 0], [0, 1], [1, 1]])
    ground = lambda x: 0
    assert not bike.check_failed(ground)
