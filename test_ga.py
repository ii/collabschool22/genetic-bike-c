from ga import GA

def test_constructor_works():
    paraboloid = lambda x: -(x[0]**2.0 + x[1]**2.0)
    bounds = [[-5.0, 5.0], [-5.0, 5.0]]
    n_iter = 100
    n_bits = 16
    n_pop = 100
    r_cross = 0.9
    r_mut = 1.0 / (float(n_bits) * len(bounds))
    ga_engine = GA(paraboloid, bounds, n_bits, n_iter, n_pop, r_cross, r_mut)
    assert ga_engine.objective([1,2]) == -5
    ga_engine.bounds

def test_run_with_paraboloid():
    paraboloid = lambda x: -(x[0]**2.0 + x[1]**2.0)
    bounds = [[-5.0, 5.0], [-5.0, 5.0]]
    n_iter = 100
    n_bits = 16
    n_pop = 100
    r_cross = 0.9
    r_mut = 1.0 / (float(n_bits) * len(bounds))
    ga_engine = GA(paraboloid, bounds, n_bits, n_iter, n_pop, r_cross, r_mut)
    solution, obj_val = ga_engine.run()
    assert 0.0 - 1e5 <= obj_val <= 0.0 + 1e5
    assert 0.0 - 1e5 <= solution[0] <= 0.0 + 1e5
    assert 0.0 - 1e5 <= solution[1] <= 0.0 + 1e5

def test_run_with_goldstein_prince():
    gp = lambda x: -(1 + (x[0]+x[1]+1)**2 * (19-14*x[0] + 3*x[0]**2 -14*x[1] + 6*x[0]*x[1]+3*x[1]**2)) * (30 + (2*x[0] -3*x[1])**2 * (18 - 32*x[0] + 12*x[0]**2 + 48*x[1] -36*x[0]*x[1] + 27*x[1]**2))
    bounds = [[-2.0, 2.0], [-2.0, 2.0]]
    n_iter = 100
    n_bits = 16
    n_pop = 100
    r_cross = 0.9
    r_mut = 1.0 / (float(n_bits) * len(bounds))
    ga_engine = GA(gp, bounds, n_bits, n_iter, n_pop, r_cross, r_mut)
    solution, obj_val = ga_engine.run()
    assert -3.0 - 1e5 <= obj_val <= -3.0 + 1e5
    assert 0.0 - 1e5 <= solution[0] <= 0.0 + 1e5
    assert -1.0 - 1e5 <= solution[1] <= -1.0 + 1e5

# Optimization problem
# https://en.wikipedia.org/wiki/Test_functions_for_optimizationy

""" from ga import GA

# objective function
#def objective(x):
#    return -(x[0]**2.0 + x[1]**2.0)


# f(x,y) = - ((x+2y-7)^² + (2x+y-5)^2)
# Optimmum point f(x=1, y= 3) = 0 
def objective_2(x):
    return -((x[0]+2*x[1]-7)**2+(2*x[0]+x[1]-5)**2)

# define range for input
bounds = [[-5.0, 5.0], [-5.0, 5.0]]

# define the total iterations
n_iter = 100

# bits per variable
n_bits = 16

# define the population size
n_pop = 100

# crossover rate
r_cross = 0.9

# mutation rate
r_mut = 1.0 / (float(n_bits) * len(bounds))

# perform the genetic algorithm search
ga_engine = GA(objective_2, bounds, n_bits, n_iter, n_pop, r_cross, r_mut)

# output of ga_engine.run will give two output, the first is list of best result, second is the log
best_res, record = ga_engine.run()


# visulize the record
ga_engine.visualize_log(ga_record=record) """

