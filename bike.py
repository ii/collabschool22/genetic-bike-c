import numpy as np

class Bike:
    def __init__(self, points, driving=1000):
        points = np.array(points).reshape((4,2))
        self.points = np.array(points, dtype=float)
        self.velocities = np.zeros_like(self.points)
        self.masses = np.array([1, 1, 40, 40])
        self.rigidity = 1e4
        self.driving = driving
        self.pairs = []
        for i in range(len(points)):
            for j in range(i+1, len(points)):
                self.pairs.append([i, j])
        self.pairs = np.array(self.pairs, dtype=int)
        point_diffs = self.points[self.pairs[:,0]] - self.points[self.pairs[:,1]]
        self.lengths = np.linalg.norm(point_diffs, axis=1)

    def spring_force(self):
        point_diffs = self.points[self.pairs[:,0]] - self.points[self.pairs[:,1]]
        lengths = np.linalg.norm(point_diffs, axis=1)
        unit_vectors = -point_diffs / lengths[:,np.newaxis]
        pair_forces = self.rigidity * (lengths - self.lengths)[:,np.newaxis] * unit_vectors
        forces = np.zeros((len(self.points), 2))
        for pair, pair_force in zip(self.pairs, pair_forces):
            forces[pair[0]] += pair_force
            forces[pair[1]] -= pair_force
        return forces

    def force(self, ground):
        forces = self.spring_force()
        forces += self.driving_force(ground)
        forces[:,1] -= self.masses * 9.81
        return forces

    def in_contact(self, ground):
        y_ground = ground(self.points[:,0])
        return self.points[:,1] <= y_ground

    def driving_force(self, ground):
        in_contact = self.in_contact(ground)
        forces = np.zeros((len(self.points), 2))
        if (in_contact[0]):
            forces[0,0] = self.driving
        return forces

    def ground_intersection(self, ground):
        for i_wheel in [0,1]:
            x = self.points[i_wheel,0]
            y_wheel = self.points[i_wheel,1]
            y_ground = ground(x)
            self.points[i_wheel,1] = max(y_wheel, y_ground)

    def get_position(self):
        return np.mean(self.points[:,0])

    def check_failed(self, ground):
        in_contact = self.in_contact(ground)
        return np.any(in_contact[2:])
