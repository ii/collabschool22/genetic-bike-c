from bike import Bike
from simulation import simulate
from plot_bike import animate_bike

initial_points = [[2, 0], [0, 0], [0.5, 1], [1.5, 1]]

simulate(initial_points, save=True)
animate_bike()
